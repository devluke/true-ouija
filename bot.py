from pathlib import Path
import json
import re

from discord.errors import Forbidden
import discord

import config

DEFAULT_DATA = {
    'question': '',
    'letters': [],
    'asker': 0,
    'last_user': 0
}

client = discord.Client()


@client.event
async def on_ready():
    await client.change_presence(activity=discord.Game(name='with spirits'))

    print(f'Member of {len(client.guilds)} guilds:')
    guilds = {}
    for g in client.guilds:
        print(f'    {g} ({g.member_count})')
    print(f'Logged in as {client.user}')


@client.event
async def on_message(msg):
    if msg.author == client.user or isinstance(msg.channel, discord.DMChannel):
        return

    path = Path(config.DATA_CONFIG['directory'], f'{msg.channel.id}.json')
    data = None
    if path.exists():
        data = json.loads(path.read_text())
    else:
        data = DEFAULT_DATA

    reset = False
    content = msg.content
    clean_msg = (content[0] + re.sub(r'[^\w]', '', content[1:])).upper()
    goodbyeTest = clean_msg == 'GOODBYE'
    letterTest = len(clean_msg) == 1
    if goodbyeTest or letterTest:
        disallowed_users = [data['asker'], data['last_user']]
        if msg.author.id in disallowed_users:
            reset = True
        elif data['question'] != '':
            if goodbyeTest:
                question = data['question']
                phrase = ''.join(data['letters'])
                reset = True
                says_phrase = f'Ouija says: {phrase}'
                embed = discord.Embed(title=question, description=says_phrase)
                asker = client.get_user(data['asker'])
                embed.set_author(name=asker, icon_url=asker.avatar_url)
                try:
                    await msg.channel.send(embed=embed)
                except Forbidden:
                    pass
            elif letterTest:
                data['letters'].append(clean_msg)
                data['last_user'] = msg.author.id
    else:
        data['question'] = msg.clean_content
        data['letters'] = []
        data['asker'] = msg.author.id
        data['last_user'] = 0

    if reset:
        data = DEFAULT_DATA
    path.write_text(json.dumps(data, separators=(',', ':')))

client.run(config.DISCORD_CONFIG['token'])
