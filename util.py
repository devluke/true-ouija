import re

from discord.errors import Forbidden

async def send(
    channel, content=None, *, tts=False, embed=None, file=None, files=None,
    delete_after=None, nonce=None
):
    try:
        await channel.send(
            content=content, tts=tts, embed=embed, file=file, files=files,
            delete_after=delete_after, nonce=nonce
        )
    except Forbidden:
        pass

async def clean_message(message):
    return re.sub(r'[^\w]', '', message)
