# True Ouija

An AskOuija bot for Discord that's more like the original subreddit than other bots

## Inviting

Follow [this link](https://discordapp.com/oauth2/authorize?client_id=580548968606597130&scope=bot&permissions=8) to invite True Ouija to your server.

## Usage

Simply ask any question and in any channel and wait for the spirits to answer! If you don't understand how to play AskOuija, read the rules on [the AskOuija subreddit](https://www.reddit.com/r/AskOuija/).

## Running

To run this bot, you first need to set some things up. Follow the steps below, creating all files and running all commands in the `true-ouija` directory:

1. Create a directory named `data`.

2. Create a file named `config.py` and paste the following into that file, replacing `YOUR-TOKEN-HERE` with your bot token:

```python
DISCORD_CONFIG = {
    'token': 'YOUR-TOKEN-HERE'
}

DATA_CONFIG = {
    'directory': 'data'
}
```

3. Create a virtual environment by running the following command:

```bash
python3.7 -m venv venv
```

4. Activate the virtual environment you created by running the following command:

```bash
source venv/bin/activate
```

5. Install the required Python libraries by running the following command:

```bash
pip install -r requirements.txt
```

6. Run the bot by running the following command:

```bash
python bot.py
```

After setting the bot up, you can run it by running the following command:

```bash
source venv/bin/activate && python bot.py
```
